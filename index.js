function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    
    if (letter.length>1){
        return undefined;
    }
    else {
       for (var i=0; i < sentence.length; i++){
            if(sentence[i] == letter) {
                result++;
                
            }
            else{
                continue;
            }
            
        }
        return result;
    }

    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let lowerText = text.toLowerCase();
    let countText = lowerText.length;

   for (var i = 0; i < countText; i++) {
        for (var j = 0; j < countText; j++) {
            if(i!=j){
                if (lowerText[i] == lowerText[j]) {
                    return false
                }
            }
        }
    }
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let dp = price*0.8;
    if(age<13){
        return undefined;
    }
    else if((age>= 13 && age < 21) || age >= 60){
        //console.log(dp.toFixed(2));
        return dp.toFixed(2);
    }
    else{
        return price.toFixed(2);
    }
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    let zeroStock = [];

    /*items.forEach(zero);*/

   /* function zero(){
        if(items.stocks=0){
            zeroStock.push(items);
        }
    }*/

    for(var i=0; i<items.length;i++){
        if(items[i].stocks==0){
            zeroStock.push(items[i].category);
            console.log(zeroStock);
        }
        else{
            continue;
        }
    }

 /*   for (var j=0; j<zeroStock.length;j++){
        if(zeroStock[j]===zeroStock.lastIndexOf(zeroStock[j])){
            console.log("hi");
            zeroStock.splice(zeroStock[i],1);
            console.log(zeroStock);
        }
    }*/

    const unique = Array.from(new Set(zeroStock));
   //zeroStock.push(items[0]);
    return unique;

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.



}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let sameVotes=[];
    let countMatch = 0;
    
    for (var i=0; i<candidateA.length;i++){
        for (var j=0; j<candidateB.length;j++){
            if(candidateB[j]==candidateA[i]){
                sameVotes.push(candidateB[j]);
                // console.log(sameVotes);
                //countMatch++;
            }
            else{
                continue;
            }
        }
    }
    //console.log(sameVotes);
    return sameVotes;
    //console.log(countMatch);
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};